exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('user_loan_request_views', (table) => {
        table.increments()
        table.integer('sale_id').notNullable().references('id').inTable('sales')
        table.specificType('loan_request_id', 'char(7)').notNullable().references('id').inTable('loan_requests')
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('user_loan_request_views')
};