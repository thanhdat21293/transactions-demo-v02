module.exports = [{
  "id": "NASDAQ",
  "type": "Vay tín chấp",
  "amount": 300000000,
  "time": 3,
  "time_unit": "nam",
  "owner_id": 7,
  "company_id": 17,
  "location_id": "DAL",
  "status": "pending",
  "expired_at": "2018-03-01T22:52:07Z",
  "created_at": "2017-07-12T11:12:53Z",
  "updated_at": "2017-12-13T19:48:25Z"
}, {
  "id": "NYSE",
  "type": "Vay tín chấp",
  "amount": 50000000,
  "time": 1,
  "time_unit": "nam",
  "owner_id": 8,
  "company_id": 8,
  "location_id": "DAN",
  "status": "cancel",
  "expired_at": "2017-05-30T18:32:48Z",
  "created_at": "2017-12-26T00:14:18Z",
  "updated_at": "2017-08-19T03:05:37Z"
}, {
  "id": "dsf22",
  "type": "Vay tín chấp",
  "amount": 30000000,
  "time": 10,
  "time_unit": "thang",
  "owner_id": 7,
  "company_id": 11,
  "location_id": "DAN",
  "status": "finish",
  "expired_at": "2018-03-12T18:40:00Z",
  "created_at": "2017-10-13T21:56:46Z",
  "updated_at": "2017-10-03T21:18:21Z"
}, {
  "id": "wq45tre",
  "type": "Vay tín chấp",
  "amount": 500000000,
  "time": 10,
  "time_unit": "nam",
  "owner_id": 12,
  "company_id": 16,
  "location_id": "HCM",
  "status": "active",
  "expired_at": "2017-05-11T03:52:45Z",
  "created_at": "2017-08-01T13:41:57Z",
  "updated_at": "2017-11-02T12:12:06Z"
}, {
  "id": "234cx43",
  "type": "Vay tín chấp",
  "amount": 10000000,
  "time": 10,
  "time_unit": "ngay",
  "owner_id": 5,
  "company_id": 16,
  "location_id": "HAD",
  "status": "cancel",
  "expired_at": "2018-03-20T13:34:07Z",
  "created_at": "2017-06-30T23:35:14Z",
  "updated_at": "2017-11-02T20:52:57Z"
}, {
  "id": "yu5wss3",
  "type": "Vay tín chấp",
  "amount": 20000000,
  "time": 4,
  "time_unit": "thang",
  "owner_id": 8,
  "company_id": 1,
  "location_id": "HAN",
  "status": "pending",
  "expired_at": "2017-12-25T09:14:18Z",
  "created_at": "2017-08-31T19:42:23Z",
  "updated_at": "2017-04-24T06:43:16Z"
}, {
  "id": "d54hgw",
  "type": "Vay tín chấp",
  "amount": 500000000,
  "time": 11,
  "time_unit": "tuan",
  "owner_id": 2,
  "company_id": 16,
  "location_id": "DAL",
  "status": "active",
  "expired_at": "2018-02-05T09:58:29Z",
  "created_at": "2017-09-02T00:14:31Z",
  "updated_at": "2017-04-03T01:21:05Z"
}, {
  "id": "wwq46gff",
  "type": "Vay tín chấp",
  "amount": 200000000,
  "time": 9,
  "time_unit": "ngay",
  "owner_id": 6,
  "company_id": 7,
  "location_id": "DAN",
  "status": "pending",
  "expired_at": "2017-09-01T17:00:07Z",
  "created_at": "2017-10-05T20:17:43Z",
  "updated_at": "2017-09-15T05:42:57Z"
}, {
  "id": "lkews4y",
  "type": "Vay tín chấp",
  "amount": 20000000,
  "time": 3,
  "time_unit": "tuan",
  "owner_id": 13,
  "company_id": 18,
  "location_id": "CAM",
  "status": "pending",
  "expired_at": "2017-06-06T14:14:04Z",
  "created_at": "2017-03-20T19:16:53Z",
  "updated_at": "2017-12-24T22:39:58Z"
}, {
  "id": "df55yg3",
  "type": "Vay tín chấp",
  "amount": 20000000,
  "time": 5,
  "time_unit": "ngay",
  "owner_id": 7,
  "company_id": 5,
  "location_id": "HCM",
  "status": "pending",
  "expired_at": "2017-06-04T00:22:25Z",
  "created_at": "2017-04-05T17:06:34Z",
  "updated_at": "2017-09-24T17:46:50Z"
}, {
  "id": "esdf56h",
  "type": "Vay tín chấp",
  "amount": 100000000,
  "time": 8,
  "time_unit": "ngay",
  "owner_id": 10,
  "company_id": 15,
  "location_id": "HAD",
  "status": "pending",
  "expired_at": "2017-07-18T04:16:08Z",
  "created_at": "2017-04-02T20:07:17Z",
  "updated_at": "2017-04-16T04:34:50Z"
}, {
  "id": "htdfr",
  "type": "Vay tín chấp",
  "amount": 500000000,
  "time": 10,
  "time_unit": "thang",
  "owner_id": 9,
  "company_id": 14,
  "location_id": "HAD",
  "status": "finish",
  "expired_at": "2017-09-03T06:06:24Z",
  "created_at": "2017-12-21T10:25:36Z",
  "updated_at": "2017-07-22T12:20:11Z"
}, {
  "id": "wer4",
  "type": "Vay tín chấp",
  "amount": 20000000,
  "time": 7,
  "time_unit": "tuan",
  "owner_id": 13,
  "company_id": 5,
  "location_id": "NHT",
  "status": "pending",
  "expired_at": "2018-01-14T11:03:21Z",
  "created_at": "2017-06-11T19:08:07Z",
  "updated_at": "2018-01-03T23:41:40Z"
}, {
  "id": "5435d",
  "type": "Vay tín chấp",
  "amount": 500000000,
  "time": 11,
  "time_unit": "nam",
  "owner_id": 20,
  "company_id": 10,
  "location_id": "CAM",
  "status": "active",
  "expired_at": "2017-11-19T05:48:58Z",
  "created_at": "2017-08-31T06:20:35Z",
  "updated_at": "2017-03-30T06:25:18Z"
}, {
  "id": "324dsf",
  "type": "Vay tín chấp",
  "amount": 50000000,
  "time": 6,
  "time_unit": "ngay",
  "owner_id": 13,
  "company_id": 19,
  "location_id": "DAL",
  "status": "cancel",
  "expired_at": "2017-05-30T12:18:28Z",
  "created_at": "2017-09-19T17:46:29Z",
  "updated_at": "2017-06-16T17:34:05Z"
}, {
  "id": "dsf324",
  "type": "Vay tín chấp",
  "amount": 300000000,
  "time": 5,
  "time_unit": "ngay",
  "owner_id": 1,
  "company_id": 11,
  "location_id": "CAM",
  "status": "pending",
  "expired_at": "2017-09-02T04:17:17Z",
  "created_at": "2017-02-12T10:15:40Z",
  "updated_at": "2017-10-21T12:39:33Z"
}, {
  "id": "sdf44",
  "type": "Vay tín chấp",
  "amount": 50000000,
  "time": 6,
  "time_unit": "tuan",
  "owner_id": 15,
  "company_id": 18,
  "location_id": "DAN",
  "status": "pending",
  "expired_at": "2017-06-30T02:37:19Z",
  "created_at": "2017-02-14T05:14:48Z",
  "updated_at": "2017-04-01T21:34:22Z"
}, {
  "id": "sdf4ff",
  "type": "Vay tín chấp",
  "amount": 20000000,
  "time": 7,
  "time_unit": "tuan",
  "owner_id": 2,
  "company_id": 12,
  "location_id": "NHT",
  "status": "cancel",
  "expired_at": "2017-10-24T20:23:48Z",
  "created_at": "2017-06-08T23:41:06Z",
  "updated_at": "2017-03-08T13:15:59Z"
}, {
  "id": "sfewr45",
  "type": "Vay tín chấp",
  "amount": 300000000,
  "time": 2,
  "time_unit": "ngay",
  "owner_id": 11,
  "company_id": 2,
  "location_id": "HAN",
  "status": "pending",
  "expired_at": "2017-04-12T02:55:35Z",
  "created_at": "2017-07-16T03:45:03Z",
  "updated_at": "2017-03-09T11:57:26Z"
}]