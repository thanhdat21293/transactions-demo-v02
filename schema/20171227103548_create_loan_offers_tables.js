exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_offers', (table) => {
        table.increments()
        table.specificType('rate', 'decimal(5, 2) CHECK (rate >= 0 AND rate <= 100)').notNullable() // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.integer('disbursement').notNullable()
        table.enum('disbursement_unit', ['ngay', 'tuan', 'thang']).notNullable()
        table.specificType('loan_request_id', 'char(7)').notNullable().references('id').inTable('loan_requests')
        table.integer('sale_id').notNullable().references('id').inTable('sales')
        table.integer('bank_id').notNullable().references('id').inTable('banks')
        table.enu('status', ['finish', 'pending', 'cancel']).notNullable().defaultTo('pending')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_offers')
};