module.exports = [
    {
        id: 1,
        name: 'ACB',
        image: 'citibank.png'
    },
    {
        id: 2,
        name: 'TPBank',
        image: 'acb.png'
    },
    {
        id: 3,
        name: 'DAF',
        image: 'acb.png'
    },
    {
        id: 4,
        name: 'SeABank',
        image: 'acb.png'
    },
    {
        id: 5,
        name: 'ABBANK',
        image: 'acb.png'
    },
    {
        id: 6,
        name: 'BacABank',
        image: 'acb.png'
    },
    {
        id: 7,
        name: 'VietCapitalBank',
        image: 'acb.png'
    },
    {
        id: 8,
        name: 'Maritime Bank',
        image: 'acb.png'
    },
    {
        id: 9,
        name: 'Techcombank',
        image: 'acb.png'
    },
    {
        id: 10,
        name: 'KienLongBank',
        image: 'acb.png'
    },
    {
        id: 11,
        name: 'Nam A Bank',
        image: 'acb.png'
    }
]