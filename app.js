const Koa = require('koa');
const Router = require('koa-router');

const redis = require('redis');
const client = redis.createClient()
const wrapper = require('co-redis');
const redisCo = wrapper(client);
const sub = redis.createClient(), pub = redis.createClient();

const app = new Koa();
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger')
let router;
if(process.env.NODE_ENV === 'production') {
	 router = Router({prefix: '/api'});
}else {
	 router = Router();
}

sub.on("subscribe", function (channel, count) {
  pub.publish("a nice channel", "I am sending a message.");
  pub.publish("a nice channel", "I am sending a second message.");
  pub.publish("a nice channel", "I am sending my last message.");
});
sub.on("message", function (channel, message) {
    console.log("sub channel " + channel + ": " + message);
});
router.get('/abc', async ctx => {
  
  sub.subscribe("a nice channel");
  })

// Database
const Locations = require('./data/locations')
const Companies = require('./data/companies')
const Users = require('./data/users')
const Banks = require('./data/banks')
const Sales = require('./data/sales')
const LoanRequests = require('./data/loan_requests')
const LoanOffers = require('./data/loan_offers')
app.use(logger())
app.use(bodyParser());
app.use(async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*");
	ctx.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	await next()
});

client.on("error", function (err) {
  console.log("Error " + err);
});

router.get('/set-default', async ctx => {
  try {
    redisCo.JSON.SET("users", '.', 1);
  }
  catch (err) {
    ctx.body = err
  }
})
router.get('/get', async ctx => {
  try {
    let a = await redisCo.JSON.GET("users");
    ctx.body = a
  }
  catch (err) {
    ctx.body = err
  }
})

router.get('/set/:value', async ctx => {
  try {
    let value = ctx.params.value
    redisCo.hmset("hosts", {a:value});
    ctx.body = value
  }
  catch (err) {
    ctx.body = err
  }
})
router.get('/get', async ctx => {
  try {
    let a = await redisCo.hgetall("hosts");
    ctx.body = a
  }
  catch (err) {
    ctx.body = err
  }
})

app.use(router.routes())
app.use(router.allowedMethods())
const port = 10003
app.listen(port, () => {
  console.log('Start http://localhost:' + port)
});
