exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_request_views', (table) => {
        table.specificType('loan_request_id', 'char(7)').notNullable().references('id').inTable('loan_requests').primary()
        table.integer('count').defaultTo(0) // Tổng view của loan item
        table.timestamp('created_at').defaultTo(knex.fn.now())
    })

};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_request_views')
};