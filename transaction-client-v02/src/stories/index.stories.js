/* eslint-disable react/react-in-jsx-scope */

import { storiesOf } from '@storybook/vue';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withNotes } from '@storybook/addon-notes';

import Button from '../components/Button.vue';
import Avatar from '../components/Avatar.vue'
import Link from '../components/Link.vue'

// Import Notes
import buttonMD from './buttonMD.md'


storiesOf('Component', module)
  .add('with some emoji', withNotes(buttonMD)(() => 
  ({
    components: { Button },
    template: `
              <div>
                <Button :class1="\' fullwidth\'" :text="\'Full width\'"></Button>
                </div>
              `,
    methods: { action: linkTo('Button') },
  })  
));

storiesOf('Button', module)
  .add('All', () => ({
    components: { Button },
    template: `
              <div>
                <Button :text="\'Default\'"></Button>
                <Button :class1="\' active\'" :text="\'Active\'"></Button>
                <Button :class1="\' btn1\'" :text="\'Button 1\'"></Button>
                <Button :class1="\' btn2\'" :text="\'Button 2\'"></Button>
                <Button :class1="\' btn3\'" :text="\'Button 3\'"></Button>
                <Button :class1="\' package\'" :text="\'Package\'"></Button>
                <Button :class1="\' end\'" :text="\'End\'"></Button>
                <Button :class1="\' disabled\'" :text="\'Disable\'"></Button>
                </div>
              `,
    methods: { action: linkTo('Button') },
  }))
  .add('Full width', () => ({
    components: { Button },
    template: `
              <div>
                <Button :class1="\' fullwidth\'" :text="\'Full width\'"></Button>
                </div>
              `,
    methods: { action: linkTo('Button') },
  }));

storiesOf('Link', module)
  .add('Default', () => ({
    components: { Link },
    template: `
              <div>
                <Link :text="\'Default\'" :href="'/default'"></Link>
              </div>
              `,
    methods: { action: linkTo('Button') },
  }))
  .add('Embed', () => ({
    components: { Link },
    template: `
              <div>
                <Link :class1="\' btn\'" :text="\'Default\'"></Link>
                <Link :class1="\' btn active\'" :text="\'Active\'"></Link>
              </div>
              `,
    methods: { action: linkTo('Button') },
  }))
  .add('Web', () => ({
    components: { Link },
    template: `
              <div>
                <Link :class1="\' btn btnweb\'" :text="\'Default\'"></Link>
                <Link :class1="\' btn btnweb btnweb1\'" :text="\'Default 1\'"></Link>
                <Link :class1="\' btn btnweb btnweb2\'" :text="\'Default 2\'"></Link>
                <Link :class1="\' btn btnweb btnweb3\'" :text="\'Default 3\'"></Link>
                <Link :class1="\' btn btnweb btnweb4\'" :text="\'Default 4\'"></Link>
                <Link :class1="\' btn btnweb disabled\'" :text="\'Disabled\'"></Link>
              </div>
              `,
    methods: { action: linkTo('Button') },
  }))

storiesOf('Avatar', module)
  .add('Multi size', () => ({
    components: { Avatar },
    template: `
              <div>
                <Avatar :width="\'120\'" :height="\'120\'" :src="\'http://icons.iconarchive.com/icons/webalys/kameleon.pics/128/Man-16-icon.png\'"></Avatar>
                <Avatar :width="\'80\'" :height="\'80\'" :src="\'http://icons.iconarchive.com/icons/webalys/kameleon.pics/128/Man-16-icon.png\'"></Avatar>
                <Avatar :width="\'40\'" :height="\'40\'" :src="\'http://icons.iconarchive.com/icons/webalys/kameleon.pics/128/Man-16-icon.png\'"></Avatar>
              </div>
              `,
    methods: { action: linkTo('Button') },
  }))
