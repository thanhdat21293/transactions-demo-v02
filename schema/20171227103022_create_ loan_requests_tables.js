exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_requests', (table) => {
        table.specificType('id', 'char(7)').notNullable().unique().notNullable().primary()
        table.enum('type', ['Vay tin chap']).notNullable()
        table.integer('amount').notNullable()
        table.integer('time').notNullable()
        table.enum('time_unit', ['day', 'week', 'month', 'year']).notNullable()
        table.integer('owner_id').notNullable().references('id').inTable('users')
        table.specificType('company_id', 'char(10)').notNullable().references('id').inTable('companies')
        table.specificType('location_id', 'char(3)').notNullable().references('id').inTable('locations')
        table.enum('status', ['finish', 'active', 'pending', 'cancel']).notNullable().defaultTo('pending')
        table.timestamp('expired_at').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_requests')
};