/* 
10000000,20000000,30000000,50000000,100000000,200000000,300000000,500000000
HAN,HCM,DAN,DAL,CAM,NHT,HAD,TNG
finish,active,pending,cancel
lei303f,lsdkfef,t23mmss,jmndbbd,lfmmd0g,nvhdeoo,irjaerl,pocmn00,rorpss9,qofk99e,lekrui0,nvbw88e,lkeif09,woekmr9,leofek9,iejf033,eo3jf8f,lei30dk,h9kdm3r,923kejf
finish,active,deal_sale,deal_user,pending,cancel
*/
const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger')
var router;
if(process.env.NODE_ENV === 'production') {
	 router = Router({prefix: '/api'});
}else {
	 router = Router();
}

// Database
const Locations = require('./data/locations')
const Companies = require('./data/companies')
const Users = require('./data/users')
const Banks = require('./data/banks')
const Sales = require('./data/sales')
const LoanRequests = require('./data/loan_requests')
const LoanOffers = require('./data/loan_offers')
app.use(logger())
app.use(bodyParser());
app.use(async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*");
	ctx.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	await next()
});


router.get('/newsfeed', async ctx => {
	let results = LoanOffers
													.filter(item => item.status === 'finish')
													.map(item => {
														let bank = Banks.find(bank => bank.id === item.bank_id)
														let loanRequest = LoanRequests.find(loanRequest => loanRequest.id === item.loan_request_id)
														return {
															rate: item.rate,
															bank_name: bank.name,
															bank_image: bank.image,
															amount: loanRequest.amount,
															time: loanRequest.time,
															time_unit: loanRequest.time_unit
														}
													})
	ctx.body = results
})

app.use(router.routes())
app.use(router.allowedMethods())

const port = 10003
app.listen(port, () => {
  console.log('Start http://localhost:' + port)
});