import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/home';
import Saleregister from '@/components/SaleRegister'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/sale-register',
      name: 'Saleregister',
      component: Saleregister
    },
  ],
});
